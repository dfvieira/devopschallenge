provider "aws" {
  region  = var.region
  version = "2.69.0"
}

module "zone-dns" {
  source = "../../modules/route53-zone"
  domain = "${var.env}.danielvieira.io"

  tags = merge(
    local.common_tags)
}

module "vpc" {
  source   = "../../modules/vpc"
  vpc_cidr = var.cidr_vpc

  tags = merge(
    local.common_tags,
    {
      "Name" = "vpc-${var.env}-${var.region}",
      "kubernetes.io/cluster/eks-${var.env}" = "shared"
    },
  )
}

module "igw" {
  source = "../../modules/igw"
  vpc_id = module.vpc.vpc_id

  tags = merge(
    local.common_tags,
    {
      "Name" = "igw-${var.env}-${var.region}"
    },
  )
}

module "eip-natgw" {
  source = "../../modules/eip"

  tags = merge(
    local.common_tags,
    {
      "Name" = "eip-nat-gateway-${var.region}"
    },
  )
}

module "natgw" {
  source    = "../../modules/natgw"
  eip_id    = module.eip-natgw.eip_id
  subnet_id = module.subnet-public.id_sub[0]

  tags = merge(
    local.common_tags,
    {
      "Name" = "natgw-${var.env}-${var.region}"
    },
  )
}

module "subnet-public" {
  source       = "../../modules/subnet"
  vpc_id       = module.vpc.vpc_id
  block_subnet = [var.public1, var.public2]
  azs          = ["${var.region}a", "${var.region}b"]

  tags = merge(
    local.common_tags,
    {
      "Name" = "subnet-public-us-${var.region}",
      "kubernetes.io/cluster/eks-${var.env}" = "shared"
    },
  )
}

module "subnet-private" {
  source       = "../../modules/subnet"
  vpc_id       = module.vpc.vpc_id
  block_subnet = [var.private1, var.private2]
  azs          = ["${var.region}a", "${var.region}b"]

  tags = merge(
    local.common_tags,
    {
      "Name" = "subnet-private-${var.region}",
      "kubernetes.io/cluster/eks-${var.env}" = "shared"
    },
  )
}

module "rtb-private" {
  source = "../../modules/rtb"
  vpc_id = module.vpc.vpc_id

  tags = merge(
    local.common_tags,
    {
      "Name" = "rtb-private-${var.region}"
    },
  )
}

module "igw-route" {
  source                 = "../../modules/route"
  route_table_id         = module.vpc.rtb_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = module.igw.igw_id
}

module "natgw-route" {
  source                 = "../../modules/route-natgw"
  route_table_id         = module.rtb-private.rtb_id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = module.natgw.id
}

module "rtb-attach-public1" {
  source         = "../../modules/rtb_attach"
  route_table_id = module.vpc.rtb_id
  subnet_id      = module.subnet-public.id_sub[0]
}

module "rtb-attach-public2" {
  source         = "../../modules/rtb_attach"
  route_table_id = module.vpc.rtb_id
  subnet_id      = module.subnet-public.id_sub[1]
}

module "rtb-attach-private1" {
  source         = "../../modules/rtb_attach"
  route_table_id = module.rtb-private.rtb_id
  subnet_id      = module.subnet-private.id_sub[0]
}

module "rtb-attach-private2" {
  source         = "../../modules/rtb_attach"
  route_table_id = module.rtb-private.rtb_id
  subnet_id      = module.subnet-private.id_sub[1]
}

module "sg_eks" {
  source = "../../modules/sg"
  name = "sg_eks_worker"
  vpc_id = module.vpc.vpc_id
  sg_ingress = {
    "22" = ["0.0.0.0/0"]
    "80" = ["0.0.0.0/0"]
  }
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.11"
}


module "eks" {
  source                               = "../../modules/eks"
  cluster_name                         = "eks-${var.env}"
  subnets                              = module.subnet-private.id_sub
  vpc_id                               = module.vpc.vpc_id
  manage_aws_auth                      = true
  worker_additional_security_group_ids = [module.sg_eks.id]
  cluster_enabled_log_types            = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  key_name                             = "eks"
  cluster_version                      = "1.16"
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access = true
  cluster_endpoint_public_access_cidrs = ["0.0.0.0/0"]
  worker_groups = [
    {
      instance_type                 = var.instance_type
      asg_max_size                  = "2"
      asg_desired_capacity          = "2"
      additional_security_group_ids = [module.sg_eks.id]
    }
  ]
  tags = {
    Owner       = "danielfpvieira@gmail.com"
    Project     = "DevOps"
    Environment = var.env
  }
}
