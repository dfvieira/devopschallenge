provider "aws" {
  region  = var.region
  version = "2.69.0"
}

module "lb_eks_apps" {
  source = "../../modules/route53-record"
  zone_id = var.zone_id
  target_domain = var.target_domain
  source_domain = var.source_domain
}