resource "aws_security_group" "this" {
  name_prefix = var.name
  vpc_id      = var.vpc_id

  dynamic "ingress" {
    for_each = var.sg_ingress
    content {
      from_port = ingress.key
      to_port   = ingress.key
      protocol  = "tcp"
      cidr_blocks = ingress.value
    }

  }
}